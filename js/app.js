(function() {
  var fields = [].slice.call(document.getElementsByClassName('field'));
  fields.forEach(function(field){
    var box = field.getElementsByClassName('field__box')[0];
    var reset = field.getElementsByClassName('field__reset')[0];
    var options_list = field.getElementsByClassName('field__options-list')[0];
    var options = [].slice.call(field.getElementsByClassName('field__option'));

    if (reset) reset.onclick = function(){
      box.value = '';
      box.focus();
    }
    if (options_list) box.onclick = function() {
      options_list.classList.add('active');
    }
    if (options) {
      options.forEach(function(option){
        option.onclick = function(){
          box.value = option.dataset.value;
          options_list.classList.remove('active');
        }
      });
    }
  });
  document.body.onclick = function(e) {
    if(!e.target.classList.contains('field__options-list') && !e.target.classList.contains('field__box')) {
      var options_lists = document.getElementsByClassName('field__options-list');
      for (var i = options_lists.length - 1; i >= 0; i--) {
        options_lists[i].classList.remove('active');
      }
    }
  }
}) ();